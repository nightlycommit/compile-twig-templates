import {glob} from "glob";
import {TwingEnvironment, TwingLoaderRelativeFilesystem, TwingSource} from "twing";
import {outputFile} from "fs-extra";
import {join, relative} from "path";
import {readFile} from "fs";

export const compileFile = (path: string, environment: TwingEnvironment): Promise<string> => {
    return new Promise((resolve, reject) => {
        readFile(path, ((error, data) => {
            if (error) {
                reject(error);
            }
            else {
                const code = environment.compileSource(
                    new TwingSource(data.toString(), path)
                );

                resolve(code);
            }
        }));
    });
};
export const compileDirectory = (
    path: string,
    outputDirectory: string
): Promise<Array<string>> => {
    return new Promise((resolve, reject) => {
        glob(`${path}/**/*.twig`, ((error, files) => {
            if (error) {
                reject(error);
            } else {
                const environment = new TwingEnvironment(
                    new TwingLoaderRelativeFilesystem()
                );

                const loadTemplates = files.map((file) => {
                    return compileFile(file, environment).then((code) => {
                        return [file, code];
                    });
                });

                resolve(Promise.all(loadTemplates)
                    .then((results) => {
                        const writeFiles = results.map(([file, code]) => {
                            return outputFile(join(outputDirectory, relative(path, file) + '.js'), code);
                        });

                        return Promise.all(writeFiles);
                    })
                    .then(() => {
                        return files;
                    })
                );
            }
        }));
    });
};