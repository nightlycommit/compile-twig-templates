import * as tape from "tape";
import {main} from "../../../src/main";
import {stub} from "sinon";
import * as compileModule from "../../../src/lib/compile";

tape('main', ({test}) => {
    test('pass root argument and outputDirectory options to compileTwigTemplates', ({end, same}) => {
        const compileDirectorySpy = stub(compileModule, "compileDirectory").callsFake(() => {
            return Promise.resolve([]);
        });

        const root = 'foo';
        const outputDirectory = 'bar';

        const argvStub = stub(process, "argv").get(() => [
            'foo',
            'bar',
            root,
            `--output-directory=${outputDirectory}`
        ]);

        main();

        same(compileDirectorySpy.callCount, 1);
        same(compileDirectorySpy.firstCall.args[0], root);
        same(compileDirectorySpy.firstCall.args[1], outputDirectory);

        compileDirectorySpy.restore();
        argvStub.restore();

        end();
    });

    test('pass root argument as outputDirectory when outputDirectory option is not provided', ({end, same}) => {
        const compileDirectorySpy = stub(compileModule, "compileDirectory").callsFake(() => {
            return Promise.resolve([]);
        });

        const root = 'foo';

        const argvStub = stub(process, "argv").get(() => [
            'foo',
            'bar',
            root
        ]);

        main();

        same(compileDirectorySpy.callCount, 1);
        same(compileDirectorySpy.firstCall.args[0], root);
        same(compileDirectorySpy.firstCall.args[1], root);

        compileDirectorySpy.restore();
        argvStub.restore();

        end();
    });
});