import * as tape from "tape";
import {compileDirectory, compileFile} from "../../../../src/lib/compile";
import {resolve} from "path";
import {TwingEnvironment, TwingLoaderNull} from "twing";
import * as fsModule from "fs-extra";
import * as globModule from "glob";
import {stub} from "sinon";

tape('Compile Module', ({test}) => {
    test('compileFile', ({test}) => {
        test('resolves in case of success', ({end, true: truthy}) => {
            return compileFile(resolve('./test/fixtures/foo.html.twig'), new TwingEnvironment(
                new TwingLoaderNull()
            )).then((code) => {
                truthy(code);

                end();
            });
        });

        test('rejects in case of error', ({end, fail, pass}) => {
            return compileFile(resolve('./test/fixtures/missing.html.twig'), new TwingEnvironment(
                new TwingLoaderNull()
            )).then(() => {
                fail();
            }).catch(() => {
                pass();
            }).finally(() => {
                end();
            });
        });
    });

    test('compileDirectory', ({test}) => {
        test('resolves in case of success', ({end, same}) => {
            const outputFileStub = stub(fsModule, "outputFile");

            return compileDirectory(
                resolve('./test/fixtures'),
                'foo'
            ).then((files) => {
                same(files, [
                    resolve('./test/fixtures/foo.html.twig')
                ], 'resolves to an array of the compiled files');

                same(outputFileStub.callCount, 1);
                same(outputFileStub.firstCall.args[0], 'foo/foo.html.twig.js', 'output in the passed output directory');

                end();
            });
        });

        test('rejects in case of error', ({end, pass, fail}) => {
            const globStub = stub(globModule, "glob").callsFake(((path: string, callback: (err: Error | null, matches: string[]) => void) => {
                callback(new Error('I am error'), []);
            }) as any);

            return compileDirectory(
                resolve('./test/fixtures'),
                'foo'
            ).then(() => {
                fail();
            }).catch(() => {
                pass();
            }).finally(() => {
                end();
            });
        });
    });
});