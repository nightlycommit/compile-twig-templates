import {Argument, createCommand, Option} from "commander";
import {compileDirectory} from "./lib/compile";

type ProgramOptions = {
    outputDirectory: string
};

const createProgram = () => createCommand('compile-twig-templates')
    .addArgument(new Argument('path').argRequired())
    .addOption(new Option('-o, --output-directory <path>'))
    .action((root, options: ProgramOptions) => {
        const outputDirectory = options.outputDirectory || root;

        return compileDirectory(root, outputDirectory).then(() => {
            return;
        });
    });

export const main = () => {
    createProgram().parse();
}